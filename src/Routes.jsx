import React from "react";
import { Switch, Route } from "react-router";
import ProductList from "./components/ProductList";
import Details from "./components/Details";
import Cart from "./components/Checkout/Index";
import Default from "./components/Default";

export default () => (
  <Switch>
    <Route exact path="/" component={ProductList} />
    <Route path="/details" component={Details} />
    <Route path="/cart" component={Cart} />
    <Route component={Default} />
  </Switch>
);
