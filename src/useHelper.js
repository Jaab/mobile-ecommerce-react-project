import { useContext, useEffect } from "react";
import { HelperContext } from "./context";

export default () => {
  const {
    handleDetail,
    addToCart,
    openModal,
    closeModal,
    increment,
    decrement,
    removeItem,
    clearCart
  } = useContext(HelperContext);

  useEffect(() => {
    if (handleDetail === undefined) {
      throw new Error(`Something seems off with handleDetail: ${handleDetail}`);
    }
    if (addToCart === undefined) {
      throw new Error(`Something seems off with addToCart: ${addToCart}`);
    }
    if (openModal === undefined) {
      throw new Error(`Something seems off with openModal: ${openModal}`);
    }
    if (closeModal === undefined) {
      throw new Error(`Something seems off with closeModal: ${closeModal}`);
    }
  }, [handleDetail, addToCart, openModal, closeModal]);

  return [
    handleDetail,
    addToCart,
    openModal,
    closeModal,
    increment,
    decrement,
    removeItem,
    clearCart
  ];
};
