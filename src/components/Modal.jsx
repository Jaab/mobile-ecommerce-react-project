import React, { useEffect } from "react";
import styled from "styled-components";
import { ButtonStyle } from "./styles";
import { Link } from "react-router-dom";
import useModal from "../useModal";
import useHelper from "../useHelper";

export default function Modal() {
  const [modal] = useModal();
  const [, , , closeModal] = useHelper();
  const { img, title, price } = modal.detail;

  useEffect(() => console.log(`modal info: \n`, modal), [modal]);

  return (
    <ModalStyle onClick={() => closeModal()}>
      <div className="container">
        <div className="row">
          <div
            id="modal"
            className="col-8 mx-auto col-md-6 col-lg-4 text-center text-capitalize p-5"
          >
            <h5>item added to cart!</h5>
            <Link to="/details">
              <img src={img} className="img-fluid" alt="product" />
            </Link>
            <h5>{title}</h5>
            <h5 className="text-muted">price : ${price}</h5>
            <Link to="/">
              <ButtonStyle onClick={() => closeModal()}>
                back
              </ButtonStyle>
            </Link>
            <Link to="/cart">
              <ButtonStyle callToAction onClick={() => closeModal()}>
                cart
              </ButtonStyle>
            </Link>
          </div>
        </div>
      </div>
    </ModalStyle>
  );
}

const ModalStyle = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  background: rgba(0, 0, 0, 0.3);
  #modal {
    background: var(--mainLight);
  }
`;
