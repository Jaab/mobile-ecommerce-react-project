import React, { useEffect } from "react";
import useInventory from "../useInventory";
import { Link } from "react-router-dom";
import { ButtonStyle } from "./styles";
import useHelper from "../useHelper";

export default () => {
  const [inventory] = useInventory();
  const [, addToCart, openModal,] = useHelper();

  const { id, title, img, price, company, info, inCart } = inventory.detailProduct;

  useEffect(() => console.log(`details updated: \n`, inventory.detailProduct), [inventory]);

  return (
    <main className="container py-5">
      <div className="col-10 mx-auto text-center text-slanted my-5">
        <h1>{title}</h1>
      </div>
      <div className="row">
        <div className="col-10 mx-auto col-md-6 my-3">
          <img src={img} className="img-fluid" alt="product" />
        </div>
      </div>
      <div className="col-10 mx-auto col-md-6 my-3">
        <h2>Model: {title}</h2>
        <h4 className="text-title text-uppercase text-muted mt-3 mb-2">
          made by: {company}
        </h4>
        <h4 className="text-blue">
          <strong>
            Price: <span>$</span>
            {price}
          </strong>
        </h4>
        <p className="text-capitalize font-weight-bold mt-3 mb-0">
          Description:
        </p>
        <p className="text-muted lead">{info}</p>
        <div>
          <Link to="/">
            <ButtonStyle>Back to Products</ButtonStyle>
          </Link>
          <ButtonStyle
            callToAction
            // disabled={inCart ? true : false}
            onClick={() => {
              addToCart(id);
              openModal(id);
            }}
          >
            {inCart ? `In Cart` : `Add To Cart`}
          </ButtonStyle>
        </div>
      </div>
    </main>
  );
};
