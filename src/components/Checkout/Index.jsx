import React from "react";
import Title from "../Title";
import CartColumn from "./CartColumn";
import useCart from "../../useCart";
import CartList from "./CartList";
import CartTotal from "./CartTotal"

export default ({history}) => {
  const [cart] = useCart();
  return (
    <>
      {!cart.length ? (
        <h1 className="text-title text-center">your cart is empty</h1>
      ) : (
        <>
          <Title name="your" title="cart" />
          <CartColumn />
          <CartList cart={cart}/>
          <CartTotal cart={cart} history={history} />
        </>
      )}
    </>
  );
};
