import React from "react";
import CartItem from "./CartItem";

export default ({ cart }) => (
    <div className="container-fluid">
      {cart.map(product => <CartItem key={product.id} product={product} />)}
    </div>
  );

