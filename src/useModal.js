import { useContext, useEffect } from "react";
import { ModalContext, SetModalContext } from "./context";

export default () => {
  const modal = useContext(ModalContext);
  const setModal = useContext(SetModalContext);

  useEffect(() => {
    if (modal === undefined) {
      throw new Error("Don't know what's in the modal...");
    }

    if (setModal === undefined) {
      throw new Error("Nothing in setModal...");
    }
  }, [modal, setModal]);

  return [modal, setModal];
};
