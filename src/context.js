import React, { createContext, useState, useEffect } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { storeProducts } from "./data";

const InventoryContext = createContext(null);
const SetInventoryContext = createContext(null);
const CartContext = createContext(null);
const SetCartContext = createContext(null);
const ModalContext = createContext(null);
const SetModalContext = createContext(null);
const HelperContext = createContext(null);

const Provider = ({ init, children }) => {
  const [inventory, setInventory] = useState(init);
  const [cart, setCart] = useState([]);

  const [modal, setModal] = useState({ isOpen: false, detail: {} });

  const getProdFromCart = id => cart.find(product => product.id === id);
  const getProdFromInventory = id =>
    inventory.storeProducts.find(product => product.id === id);
  const getProdFromData = id =>
    storeProducts.find(product => product.id === id);

  const resetProducts = () => {
    let resetProds = [];
    storeProducts.forEach(product => {
      if (product.inCart) product.inCart = false;
      resetProds = [...resetProds, product];
    });
    setInventory({ storeProducts: resetProds });
  };

  //----

  const openModal = id => {
    const product = getProdFromInventory(id);
    setModal({ ...modal, isOpen: true, detail: { ...product } });
  };

  const closeModal = () => setModal({ ...modal, isOpen: false });

  //----

  const handleDetail = id => {
    const product = getProdFromData(id);
    setInventory({ ...inventory, detailProduct: product });
  };

  //----

  const addToCart = id => {
    let updateProds = [...inventory.storeProducts];
    let index = updateProds.indexOf(getProdFromInventory(id));
    let product = updateProds[index];
    if (!product.inCart) {
      product.inCart = true;
    }
    ++product.count;
    product.total = product.count * product.price;
    // if (cart) {
    //   if (id) {
    //     let updatedCart = cart.filter(product => product.id !== id);
    //     setCart([product, ...updatedCart]);
    //   }
    // }
    if (cart.includes(product)) console.log(product);
    setCart([product, ...cart]);
    setInventory({
      ...inventory,
      storeProducts: [...updateProds],
      detail: { ...product }
    });
  };

  const increment = id => {
    let tempCart = [...cart];
    const selectedProduct = tempCart.find(prod => prod.id === id);
    const index = tempCart.indexOf(selectedProduct);
    let product = tempCart[index];
    product.count = product.count + 1;
    product.total = product.count * product.price;
    setCart([...tempCart]);
  };

  const decrement = id => {
    let tempCart = [...cart];
    const selectedProduct = tempCart.find(prod => prod.id === id);
    const index = tempCart.indexOf(selectedProduct);
    let product = tempCart[index];
    product.count = product.count - 1;
    if (product.count === 0) {
      removeItem(id);
    } else {
      product.total = product.count * product.price;
      setCart([...tempCart]);
    }
  };

  const removeItem = id => {
    const { storeProducts } = inventory;
    let cartWithoutItem = cart.filter(product => product.id !== id);
    let removedItem = getProdFromCart(id);
    removedItem.inCart = false;
    removedItem.count = 0;
    removedItem.total = 0;
    console.log(`removed: `, removedItem);
    setCart(cartWithoutItem);
    setInventory({ ...inventory, storeProducts: [...storeProducts] });
  };

  const clearCart = () => {
    setCart([]);
    resetProducts();
  };

  //----

  const helper = {
    handleDetail,
    addToCart,
    openModal,
    closeModal,
    increment,
    decrement,
    removeItem,
    clearCart
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => resetProducts(), []);
  useEffect(() => console.log(`inventory updated: \n`, inventory), [inventory]);
  useEffect(() => console.log(`cart updated: \n`, cart), [cart]);
  useEffect(() => console.log(`modal updated: \n`, modal), [modal]);

  return (
    <InventoryContext.Provider value={inventory}>
      <SetInventoryContext.Provider value={setInventory}>
        <CartContext.Provider value={cart}>
          <SetCartContext.Provider value={setCart}>
            <ModalContext.Provider value={modal}>
              <SetModalContext.Provider value={setModal}>
                <HelperContext.Provider value={helper}>
                  <Router>{children}</Router>
                </HelperContext.Provider>
              </SetModalContext.Provider>
            </ModalContext.Provider>
          </SetCartContext.Provider>
        </CartContext.Provider>
      </SetInventoryContext.Provider>
    </InventoryContext.Provider>
  );
};

export {
  Provider,
  InventoryContext,
  SetInventoryContext,
  CartContext,
  SetCartContext,
  ModalContext,
  SetModalContext,
  HelperContext
};
