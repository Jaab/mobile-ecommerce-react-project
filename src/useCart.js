import { useContext, useEffect } from "react";
import { CartContext, SetCartContext } from "./context";

export default () => {
  const cart = useContext(CartContext);
  const setCart = useContext(SetCartContext);

  useEffect(() => {
    if (cart === undefined) {
      throw new Error("Don't know what's in the cart...");
    }

    if (setCart === undefined) {
      throw new Error("Nothing in setCart...");
    }
  }, [cart, setCart]);

  return [cart, setCart];
};
